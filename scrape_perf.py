import sys
import glob
import re

import pandas as pd

import os


name_pattern=re.compile(
    "^(?P<code_name>[^-_]*)-gpus=(?P<gpus>[0-9]*)_mx1=(?P<mx1>[0-9]*)_mx2=(?P<mx2>[0-9]*)_mx3=(?P<mx3>[0-9]*)(?:_sl=(?P<sl>[-0-9]*))?(?:_us=(?P<us>[0-1]*))?")

perf_pattern=re.compile(
    "^zone-cycles/omp_wsecond = (?P<perf>[+-]?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+))$")
#Get all sim names
data_dir=sys.argv[1]
sim_names=[ d for d in os.listdir(data_dir) if os.path.isdir(f"{data_dir}/{d}") ]
sim_names.sort()

#Parse data names
code_names={ sim_name: name_pattern.match(sim_name).group("code_name")
        for sim_name in sim_names}
gpuss={ sim_name: name_pattern.match(sim_name).group("gpus")
        for sim_name in sim_names}
mx1s={ sim_name: name_pattern.match(sim_name).group("mx1")
        for sim_name in sim_names}
mx2s={ sim_name: name_pattern.match(sim_name).group("mx2")
        for sim_name in sim_names}
mx3s={ sim_name: name_pattern.match(sim_name).group("mx3")
        for sim_name in sim_names}
sls={ sim_name: name_pattern.match(sim_name).group("sl")
        for sim_name in sim_names}
uss={ sim_name: name_pattern.match(sim_name).group("us")
        for sim_name in sim_names}

perfs={sim_name:None for sim_name in sim_names}

for sim_name in sim_names:

    output_name = f"{data_dir}/{sim_name}/output.txt"

    if os.path.isfile(output_name):

        with open(output_name,"r") as f:
            perf_matches = [ perf_pattern.match(line) for line in f if perf_pattern.match(line) is not None]

            if len(perf_matches) > 0:
                perfs[sim_name] = perf_matches[0].group("perf")
            else:
                #print(f"{output_name} failed" )
                pass

        #print(code_names[sim_name],gpuss[sim_name],mx1s[sim_name],mx2s[sim_name],mx3s[sim_name],sls[sim_name],uss[sim_name],perfs[sim_name])

data = {"sim_name":sim_names,
        "code_name":[ code_names[sim_name] for sim_name in sim_names],
        "gpus":     [ gpuss[sim_name]      for sim_name in sim_names],
        "mx1":      [ mx1s[sim_name]       for sim_name in sim_names],
        "mx2":      [ mx2s[sim_name]       for sim_name in sim_names],
        "mx3":      [ mx3s[sim_name]       for sim_name in sim_names],
        "sl" :      [ sls[sim_name]        for sim_name in sim_names],
        "us" :      [ uss[sim_name]        for sim_name in sim_names],
        "perf":     [ perfs[sim_name]      for sim_name in sim_names],
        }
df = pd.DataFrame.from_dict(data)
df.to_csv("perf.csv")
