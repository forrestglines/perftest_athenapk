import matplotlib

matplotlib.use('Agg')

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


df= pd.read_csv("perf.csv")

#Plot AthenaPK performance, all on one plot

#Plot us=0 (no scratch) sl=0 (scratch level 0 ) sl=1 (scratch level 1) as three lines

fig,ax = plt.subplots()

no_scratch = df.loc[(df["us"] == 0) & (df["code_name"] == "athenaPK")].sort_values(by=["mx1"])
ax.plot(no_scratch["mx1"],no_scratch["perf"],label="No Scratch",marker="o")

scratch0 = df.loc[(df["sl"] == 0) & (df["code_name"] == "athenaPK")].sort_values(by=["mx1"])
ax.plot(scratch0["mx1"],scratch0["perf"],label="Scratch Level 0",marker="o")

scratch1 = df.loc[(df["sl"] == 1) & (df["code_name"] == "athenaPK")].sort_values(by=["mx1"])
ax.plot(scratch1["mx1"],scratch1["perf"],label="Scratch Level 1",marker="o")

ax.legend()

ax.set_title("AthenaPK Performance")

ax.set_xscale("log",basex=2)
ax.set_yscale("log")

ax.xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())
ax.set_xlabel("Mesh block Length")
ax.set_ylabel("Cell Cycles per Second")

fig.savefig("athenaPK_perf.pdf")
fig.savefig("athenaPK_perf.png")

plt.close(fig)

fig,ax = plt.subplots()

parthenon = df.loc[df["code_name"] == "parthenon"].sort_values(by=["mx1"])
ax.plot(parthenon["mx1"],parthenon["perf"],label="Parthenon",marker="o")

ax.set_title("Parthenon Performance")

ax.set_xscale("log",basex=2)
ax.set_yscale("log")

ax.xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())
ax.set_xlabel("Mesh block Length")
ax.set_ylabel("Cell Cycles per Second")

fig.savefig("parthenon_perf.pdf")
fig.savefig("parthenon_perf.png")

plt.close(fig)
