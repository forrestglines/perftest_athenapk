#!/bin/bash

# Begin LSF Directives
#BSUB -W 1:00
#BSUB -nnodes 1

total_nodes=1
gpus_per_node=1

function log2 {
    local x=0
    for (( y=$1-1 ; $y > 0; y >>= 1 )) ; do
        let x=$x+1
    done
    echo $x
}

total_gpus=$(( $gpus_per_node*$total_nodes ))
log2_total_gpus=$(log2 total_gpus)

data_dir="/gpfs/alpine/ast146/scratch/glinesfo/perfTest_athenaPK"

PARTHENON_EXE="$data_dir/advection-example"
ATHENA_PK_EXE="$data_dir/athenaPK"

ATHENA_PK_INPUT="/autofs/nccs-svm1_home1/glinesfo/code/perfTest_athenaPK/linear_wave3d.in"
PARTHENON_INPUT="/autofs/nccs-svm1_home1/glinesfo/code/perfTest_athenaPK/parthinput.advection"

MPI_RUN="jsrun -a 1 -c 1 -g 1 -d packed --smpiargs="-gpu" -E LD_PRELOAD=/opt/ibm/spectrum_mpi/lib/pami_433/libpami.so"

#Set minimum meshblock size (constrained by memory)
min_mx1=16
min_mx2=16
min_mx3=16

#Set maximum meshblock size (constrained by memory)
max_mx1=256
max_mx2=256
max_mx3=256

#How many scratch levels
scratch_levels=2

for (( log2_gpus=0; log2_gpus<=log2_total_gpus; log2_gpus++ ))
do

    #Calculate number of max_nx meshblocks can fit on $gpus (only do powers of 2)
    gpus=$(( 2**$log2_gpus ))
    min_mb1=$(( 2**($log2_gpus/3 + ($log2_gpus%3>=1)) ))
    min_mb2=$(( 2**($log2_gpus/3 + ($log2_gpus%3>=2)) ))
    min_mb3=$(( 2**($log2_gpus/3) )) 

    echo "Testing $gpus gpus arranged $min_mb1 x $min_mb2 x $min_mb3"
    
    #Calculate total mesh size
    nx1=$(( $max_mx1*$min_mb1 ))
    nx2=$(( $max_mx2*$min_mb2 ))
    nx3=$(( $max_mx3*$min_mb3 ))

    for (( mx1=$min_mx1,mx2=$min_mx2,mx3=$min_mx3; \
           mx1<=$max_mx1,mx2<=$max_mx2,mx3<=$max_mx3; \
           mx1*=2,mx2*=2,mx3*=2 ))
    do
        echo "    Runnning with meshblocks $mx1 x $mx2 x $mx3"

        code_name="parthenon"
        code_exe=$PARTHENON_EXE
        code_input=$PARTHENON_INPUT
        sim_name="${code_name}-gpus=${gpus}_mx1=${mx1}_mx2=${mx2}_mx3=${mx3}"
        sim_dir="${data_dir}/${sim_name}"

        mkdir -p ${sim_dir}
        (
            echo "    cd $sim_dir"
            cd $sim_dir

            cmd="   $MPI_RUN -n ${gpus} ${code_exe} -i ${code_input}\
                parthenon/mesh/nx1=$nx1 parthenon/mesh/nx2=$nx2 parthenon/mesh/nx3=$nx3\
                parthenon/meshblock/nx1=$mx1 parthenon/meshblock/nx2=$mx2 parthenon/meshblock/nx3=$mx3\
                >& output.txt"
            echo "$cmd"
            eval $cmd
            echo
        )


        code_name="athenaPK"
        code_exe=$ATHENA_PK_EXE
        code_input=$ATHENA_PK_INPUT

        for (( scratch_level=-1; scratch_level<scratch_levels; scratch_level++ ))
        do
            use_scratch=$(( $scratch_level >= 0 ))

            sim_name="${code_name}-gpus=${gpus}_mx1=${mx1}_mx2=${mx2}_mx3=${mx3}_sl=${scratch_level}_us=${use_scratch}"
            sim_dir="${data_dir}/${sim_name}"

            mkdir -p ${sim_dir}
            (
                #echo "    cd $sim_dir"
                cd $sim_dir
                cmd="    $MPI_RUN -n ${gpus} ${code_exe} -i ${code_input}\
                    parthenon/mesh/nx1=$nx1 parthenon/mesh/nx2=$nx2 parthenon/mesh/nx3=$nx3\
                    parthenon/meshblock/nx1=$mx1 parthenon/meshblock/nx2=$mx2 parthenon/meshblock/nx3=$mx3\
                    hydro/use_scratch=${use_scratch} hydro/scratch_level=${scratch_level}\
                    >& output.txt"
                #echo "$cmd"
                #eval $cmd
                #echo
            )
        done
    done
done
